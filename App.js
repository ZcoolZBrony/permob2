import React, { useState, Component, useEffect } from "react";
import {StyleSheet, Text,  View,  Image,  TextInput,  Button,  TouchableOpacity,  Alert,  SafeAreaView} from 'react-native';
import { createAppContainer, NavigationContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

const titleText = "Hello Buddy";
const bodyText = "Click The button if want to continue to login page";
const [email, setEmail] = ("");
const [password, setPassword] = ("");

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        
        <Text style={styles.baseText}>
          <Text style={styles.titleText}>
            {titleText}
            {"\n"}
          </Text>
        </Text>

        <Image style={styles.image} source={require("./asset/izzy_icon.jpeg")} />

        <Text style={styles.baseText}>
          <Text style={styles.smallText}>
          {bodyText}
          </Text>
        </Text>

        <TouchableOpacity style={styles.loginBtn} onPress={() => this.props.navigation.navigate('Details')}>
          <Text style={styles.loginText}>Welcome</Text>
        </TouchableOpacity>

      </View>
    );
  }
}

class DetailsScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.baseText}>
          <Text style={styles.titleText}>
            {titleText}
            {"\n"}
            {"\n"}
           </Text>
        </Text>
        <Image style={styles.image} source={require("./asset/izzy_icon.jpeg")} />

      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Email."
          placeholderTextColor="#003f5c"
          onChangeText={(email) => setEmail(email)}
        />
      </View>

      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Password."
          placeholderTextColor="#003f5c"
          secureTextEntry={true}
          onChangeText={(password) => setPassword(password)}
        />
      </View>

        <TouchableOpacity style={styles.loginBtn} onPress={() => this.props.navigation.navigate('Home')}>
           <Text style={styles.loginText}>Welcome</Text>
        </TouchableOpacity>

        </View>
    );
  }
}

const AppNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen,
  },
  {
    initialRouteName: 'Home',
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#A0E7E5",
    alignItems: "center",
    justifyContent: "center",
  },

  image: {
    marginBottom: 40,
    width: 120,
    height: 120,
    resizeMode: 'contain'
  },

  inputView: {
    backgroundColor: "#FFC0CB",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,

    alignItems: "center",
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
  },

  forgot_button: {
    height: 30,
    marginBottom: 30,
  },

  loginBtn: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: "#FF1493",
  },

  baseText: {
    textAlign: 'center',
    fontFamily: "Cochin"
  },

  titleText: {
    fontSize: 20,
    fontWeight: "bold"
  },

  smallText: {
    fontSize: 16,
  }
});

export default createAppContainer(AppNavigator);